using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode] // Be aware of issues in unity editor
public class ApplyShader : MonoBehaviour
{
    public Material shaderMat;

    void OnRenderImage(RenderTexture src, RenderTexture dest)
    {
        Graphics.Blit(src, dest, shaderMat); // Blit copy/pastes but applies shader
    }
}
