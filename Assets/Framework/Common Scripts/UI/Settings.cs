﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{
    public Toggle InvertYAxis;
    public Slider MainVolume;
    public Slider SFXVolume;
    public Slider VOVolume;
    public Slider MusicVolume;
    public Slider GraphicsQuality;
    public TMPro.TMP_Text GraphicsQualityDisplay;

    protected bool IsLoading = false;

    public void LoadSettings()
    {
        IsLoading = true;

        float mainVolume = PlayerPrefs.GetFloat("Volume.Main", 50.0f);
        AudioShim.SetRTPCValue("Volume_Master", mainVolume);
        MainVolume.SetValueWithoutNotify(mainVolume);

        float sfxVolume = PlayerPrefs.GetFloat("Volume.SFX", 50.0f);
        AudioShim.SetRTPCValue("Volume_SFX", sfxVolume);
        SFXVolume.SetValueWithoutNotify(sfxVolume);

        float voVolume = PlayerPrefs.GetFloat("Volume.VO", 50.0f);
        AudioShim.SetRTPCValue("Volume_VO", voVolume);
        VOVolume.SetValueWithoutNotify(voVolume);

        float musicVolume = PlayerPrefs.GetFloat("Volume.Music", 50.0f);
        AudioShim.SetRTPCValue("Volume_Music", musicVolume);
        MusicVolume.SetValueWithoutNotify(musicVolume);

        InvertYAxis.isOn = PlayerPrefs.GetInt("Controls.InvertY", 0) == 1;

        // setup the graphics qualities
        string[] names = QualitySettings.names;
        GraphicsQuality.minValue = 0;
        GraphicsQuality.maxValue = names.Length - 1;
        int qualityLevel = PlayerPrefs.GetInt("QualityLevel", QualitySettings.GetQualityLevel());
        GraphicsQuality.SetValueWithoutNotify(qualityLevel);
        QualitySettings.SetQualityLevel(qualityLevel, true);
        GraphicsQualityDisplay.text = "Graphics Quality (" + names[QualitySettings.GetQualityLevel()] + ")";

        IsLoading = false;
    }

    public void InvertYChanged(bool newValue)
    {
        if (IsLoading)
            return;

        PlayerPrefs.SetInt("Controls.InvertY", newValue ? 1 : 0);
        PlayerPrefs.Save();
    }

    public void VolumeChanged_Master(float newValue)
    {
        if (IsLoading)
            return;

        PlayerPrefs.SetFloat("Volume.Main", newValue);
        PlayerPrefs.Save();

        AudioShim.SetRTPCValue("Volume_Master", newValue);
    }

    public void VolumeChanged_SFX(float newValue)
    {
        if (IsLoading)
            return;

        PlayerPrefs.SetFloat("Volume.SFX", newValue);
        PlayerPrefs.Save();

        AudioShim.SetRTPCValue("Volume_SFX", newValue);
    }

    public void VolumeChanged_VO(float newValue)
    {
        if (IsLoading)
            return;

        PlayerPrefs.SetFloat("Volume.VO", newValue);
        PlayerPrefs.Save();

        AudioShim.SetRTPCValue("Volume_VO", newValue);
    }

    public void VolumeChanged_Music(float newValue)
    {
        if (IsLoading)
            return;

        PlayerPrefs.SetFloat("Volume.Music", newValue);
        PlayerPrefs.Save();

        AudioShim.SetRTPCValue("Volume_Music", newValue);
    }

    public void GraphicsQualityChanged(float newValue)
    {
        QualitySettings.SetQualityLevel(Mathf.RoundToInt(GraphicsQuality.value), true);
        PlayerPrefs.SetInt("QualityLevel", Mathf.RoundToInt(GraphicsQuality.value));

        string[] names = QualitySettings.names;
        GraphicsQualityDisplay.text = "Graphics Quality (" + names[Mathf.RoundToInt(GraphicsQuality.value)] + ")";

        PlayerPrefs.Save();
    }

}
