﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsGroup : MonoBehaviour
{
    public bool IsExpanded = false;
    public GameObject TargetGroup;
    public TMPro.TMP_Text ButtonText;
    public string BaseText;

    private List<SettingsGroup> OtherGroups;

    // Start is called before the first frame update
    void Start()
    {
        TargetGroup.SetActive(IsExpanded);

        OtherGroups = new List<SettingsGroup>(FindObjectsOfType<SettingsGroup>());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ToggleExpanded(bool updateOthers = true)
    {
        IsExpanded = !IsExpanded;

        // do we need to collapse other groups?
        if (IsExpanded && updateOthers)
        {
            foreach(var group in OtherGroups)
            {
                // skip ourselves
                if (group == this)
                    continue;

                // skip if the group is not expanded
                if (!group.IsExpanded)
                    continue;

                group.ToggleExpanded(false);
            }
        }

        TargetGroup.SetActive(IsExpanded);

        ButtonText.text = "[ " + (IsExpanded ? "-" : "+") + " ] " + BaseText;
    }
}
