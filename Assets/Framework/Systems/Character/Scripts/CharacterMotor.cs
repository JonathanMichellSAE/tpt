﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

[RequireComponent(typeof(Rigidbody))]
public class CharacterMotor : MonoBehaviour, IPausable
{
    [Header("Configuration")]
    public CharacterMotorConfig Config;
    public Transform CameraAnchor;

    [Header("Events")]
    public UnityEvent OnPlayFootstepAudio;
    public UnityEvent OnPlayJumpAudio;
    public UnityEvent OnPlayDoubleJumpAudio;
    public UnityEvent OnPlayLandAudio;

    public UnityEvent OnToggleSettingsMenu;

    [Header("Animations")]
    public Animator CharacterAnimController;
    public string Flag_IsWalking = "isWalking";
    public float animDamperTime = 0.2f;

    [Header("Settings")]
    public bool brendansInvertCameraBool = false;

    [Header("Debugging")]
    public bool DEBUG_ShowStepRays = false;

    protected bool IsRunning = false;
    protected bool IsGrounded = true;
    protected bool IsJumping = false;

    protected int JumpCount = 0;
    protected float JumpTargetY = float.MinValue;
    protected float OriginalDrag = 0f;

    protected bool LockCursor = true;
    protected bool EnableUpdates = true;
    protected bool CanShowSettings = true;

    protected float TimeUntilNextFootstep = -1f;

    protected Rigidbody CharacterRB;
    protected Collider CharacterCollider;
    protected CinemachineVirtualCamera Camera;
    protected float CameraPitch = 0f;

    protected bool IsCameraLocked
    {
        get
        {
            return !Cursor.visible && Cursor.lockState == CursorLockMode.Locked;
        }
    }

    protected Vector2 currentMoveInput;
    protected Vector2 previousMoveInput;

    protected virtual void Start()
    {
        CharacterRB = GetComponent<Rigidbody>();
        CharacterCollider = GetComponent<Collider>();
        Camera = GetComponentInChildren<CinemachineVirtualCamera>();

        OriginalDrag = CharacterRB.drag;
        CharacterCollider.material = Config.DefaultMaterial;

        UpdateCursorLock(LockCursor);
        
        CameraAnchor.transform.position = transform.position + Config.CameraHeight * Vector3.up;

        currentMoveInput = GetMovementInput();
        previousMoveInput = currentMoveInput;
    }

    #region Input Manager
    protected Vector2 _Internal_MovementInput;
    public void OnMove(InputValue value)
    {
        _Internal_MovementInput = value.Get<Vector2>();
    }    

    protected Vector2 _Internal_LookInput;
    public void OnLook(InputValue value)
    {
        _Internal_LookInput = value.Get<Vector2>();
    }  

    protected bool _Internal_JumpInput;
    public void OnJump(InputValue value)
    {
        _Internal_JumpInput = value.Get<float>() > 0.5f;
    }

    protected bool _Internal_SprintInput;
    public void OnSprint(InputValue value)
    {
        _Internal_SprintInput = value.Get<float>() > 0.5f;
    }

    public void OnSettings(InputValue value)
    {
        if(CanShowSettings)
        OnToggleSettingsMenu?.Invoke();
    }    
    #endregion

    #region Camera Handling
    protected virtual Vector2 GetCameraInput()
    {
        //if (Config.RestrictVerticalRotation)
        //{
        //    //return new Vector2(Mathf.Clamp(_Internal_LookInput.x * Config.CameraSensitivity_Horizontal, Config.VerticalRotation_Min, Config.VerticalRotation_Max),
        //    //       _Internal_LookInput.y * Config.CameraSensitivity_Vertical * (Config.InvertCamera_Vertical ? 1f : -1f));

        //    return new Vector2(_Internal_LookInput.x * Config.CameraSensitivity_Horizontal,
        //            Mathf.Clamp(_Internal_LookInput.y * Config.CameraSensitivity_Vertical * (Config.InvertCamera_Vertical ? 1f : -1f), Config.VerticalRotation_Min, Config.VerticalRotation_Max));
        //}
        
        return new Vector2(_Internal_LookInput.x * Config.CameraSensitivity_Horizontal,
                   _Internal_LookInput.y * Config.CameraSensitivity_Vertical * (Config.InvertCamera_Vertical ? 1f : -1f));
    }

    protected virtual void Update()
    {
        CameraAnchor.transform.position = transform.position + Config.CameraHeight * Vector3.up;

        // do nothing if updating is turned off or if paused
        if (!EnableUpdates || PauseManager.IsPaused)
            return;

        // retrieve the camera input (already has sensitivity and inversion applied)
        Vector2 cameraInput = GetCameraInput();

        // update the character rotation
        if (Config.CameraMode == CharacterMotorConfig.ECameraMode.FirstPerson)
        {
            // determine the new rotations
            Quaternion newCharacterRotation = transform.localRotation * Quaternion.Euler(0f, cameraInput.x, 0f);
            Quaternion newCameraRotation = Camera.transform.localRotation * Quaternion.Euler(cameraInput.y, 0f, 0f);

            transform.localRotation = newCharacterRotation;
            Camera.transform.localRotation = newCameraRotation;
        }
        else if (Config.CameraMode == CharacterMotorConfig.ECameraMode.ThirdPerson)
        {
            CameraAnchor.transform.localRotation *= Quaternion.Euler(0, cameraInput.x, 0f);
            if (brendansInvertCameraBool)
            {
                CameraPitch = Mathf.Clamp(CameraPitch + -cameraInput.y, Config.VerticalRotation_Min, Config.VerticalRotation_Max);
            }
            else
            {
                CameraPitch = Mathf.Clamp(CameraPitch + cameraInput.y, Config.VerticalRotation_Min, Config.VerticalRotation_Max);
            }
            Vector3 v3 = CameraAnchor.transform.eulerAngles;
            v3.x = CameraPitch;
            v3.z = 0;
            CameraAnchor.eulerAngles = v3;
        }
    }
    #endregion

    #region Movement Handling
    protected virtual void UpdateRunning()
    {
        if (!IsGrounded)
        {
            IsRunning = false;
            return;
        }

        if (Config.RunMode == CharacterMotorConfig.ERunMode.Hold)
            IsRunning = _Internal_SprintInput;
        else if (Config.RunMode == CharacterMotorConfig.ERunMode.Toggle)
        {
            if (!IsRunning && _Internal_SprintInput)
                IsRunning = true;
        }
    }

    protected virtual RaycastHit UpdateIsGrounded()
    {
        // raycast to check where the ground is
        RaycastHit hitResult;     
        float groundCheckDistance = Config.GroundedThreshold + (Config.CharacterHeight * 0.5f) - Config.CharacterRadius;
        float workingRadius = Config.CharacterRadius * (1f - Config.CollisionBuffer);
        if (Physics.SphereCast(transform.position, workingRadius, Vector3.down, out hitResult, groundCheckDistance, Config.WalkableMask, QueryTriggerInteraction.Ignore))
        {
            // check if the character is grounded
            IsGrounded = true;
        }
        else
            IsGrounded = false;

        return hitResult;
    }

    protected virtual void FixedUpdate()
    {
        // do nothing if updating is turned off or if paused
        if (!EnableUpdates || PauseManager.IsPaused)
            return;

        // Update if grounded
        bool wasGrounded = IsGrounded;
        RaycastHit hitResult = UpdateIsGrounded();

        // Update if running
        UpdateRunning();

        // read the movement input
        Vector2 movementInput = GetMovementInput();

        // if in third person AND we're trying to move then we need to rotate the character
        if (movementInput.sqrMagnitude > 0 && Config.CameraMode == CharacterMotorConfig.ECameraMode.ThirdPerson)
        {
            Vector3 forward2D = CameraAnchor.forward;
            forward2D.y = 0;
            
            Quaternion targetRotation = Quaternion.LookRotation(forward2D.normalized, Vector3.up);
            transform.rotation = targetRotation;

            CameraAnchor.transform.position = transform.position + Config.CameraHeight * Vector3.up;
        }

        // calculate the potential movement vector (handles grounded, not grounded and air control on or off)
        Vector3 movementVector = transform.forward * movementInput.y * CurrentSpeed + 
                                 transform.right   * movementInput.x * CurrentSpeed;
        float originalMagnitude = movementVector.magnitude;

        // if we're grounded then determine how far we move
        if (IsGrounded)
        {
            // project the movement along the plane
            movementVector = Vector3.ProjectOnPlane(movementVector, hitResult.normal);
            movementVector = movementVector.normalized * originalMagnitude;

            // check if moving up and amount is above the slope limit
            if (movementVector.y > float.Epsilon && Vector3.Angle(Vector3.up, hitResult.normal) >= Config.MaxSlope)
            {
                movementVector = Vector3.zero;
            }

            // update the Is Walking flag
            //CharacterAnimController?.SetBool(Flag_IsWalking, movementInput.sqrMagnitude > 0);
        }

        if (currentMoveInput != movementInput)
        {
            previousMoveInput = currentMoveInput;
            currentMoveInput = movementInput;
        }

        // update the Is Walking flag
        if (CharacterAnimController != null)
        {
            CharacterAnimController.SetBool(Flag_IsWalking, movementInput.sqrMagnitude > 0.01);

            //Not sure if there's a better way of doing this - Just wanting to force either -1, 0 or 1 (and nothing inbetween)
            float moveX;
            float moveY;

            if (movementInput.x > 0)
                moveX = 1;
            else if (movementInput.x < 0)
                moveX = -1;
            else
                moveX = 0;

            if (movementInput.y > 0)
                moveY = 1;
            else if (movementInput.y < 0)
                moveY = -1;
            else
                moveY = 0;

            // Need to smooth out the movement input by lerping previous input to new input
            CharacterAnimController.SetFloat("horizontal_f", moveX, animDamperTime, Time.deltaTime);
            CharacterAnimController.SetFloat("vertical_f", moveY, animDamperTime, Time.deltaTime);

            //CharacterAnimController.SetFloat("horizontal_f", Mathf.Lerp(previousMoveInput.x, currentMoveInput.x, 0.5f));
            //CharacterAnimController.SetFloat("horizontal_f", Mathf.Lerp(previousMoveInput.y, currentMoveInput.y, 0.5f));
        }

        // has a jump been requested?
        bool jumpRequested = Config.CanJump && _Internal_JumpInput;

        // jump can only happen if either we're grounded or we're on the first jump of a double jump
        jumpRequested &= IsGrounded || (Config.CanDoubleJump && (JumpCount < 2));

        // are we at rest?
        if (!jumpRequested && !IsJumping && IsGrounded && CharacterRB.velocity.sqrMagnitude < 0.1f && movementInput.sqrMagnitude < 0.1f)
        {
            CharacterRB.velocity = Vector3.zero;
            CharacterRB.Sleep();
            IsRunning = false;
        }
        else
        {
            // update the character's velocity if on ground or can air control
            if (IsGrounded)
                CharacterRB.velocity = Vector3.Lerp(CharacterRB.velocity, movementVector, Config.MovementBlendRate);
            else if (Config.AirControl)
            {
                movementVector.y = CharacterRB.velocity.y;
                CharacterRB.velocity = Vector3.Lerp(CharacterRB.velocity, movementVector, Config.MovementBlendRate);
            }

            // was a jump requested
            if (jumpRequested)
            {
                // handle jumping audio
                if (JumpCount == 0)
                    OnPlayJumpAudio?.Invoke();
                else
                    OnPlayDoubleJumpAudio?.Invoke();

                ++JumpCount;
                IsJumping = true;
                CharacterRB.drag = 0;
                CharacterCollider.material = Config.MaterialWhenJumping;
                JumpTargetY = transform.position.y + Config.JumpHeight;
            }

            // are we jumping?
            if (IsJumping)
            {
                Vector3 velocity = CharacterRB.velocity;
                velocity.y = Mathf.Lerp(velocity.y, Config.JumpVelocity, Config.JumpVelocityBlend);
                CharacterRB.velocity = velocity;

                // have we reached the maximum height?
                if (transform.position.y >= JumpTargetY)
                    IsJumping = false;
            }
            else if (!IsGrounded)
            {
                CharacterRB.AddForce(Vector3.down * Config.FallForce, ForceMode.Impulse);
            }
            else
            {
                // if we weren't grounded previously then play the land audio and reset the footstep interval
                if (!wasGrounded)
                {
                    OnPlayLandAudio?.Invoke();
                    TimeUntilNextFootstep = Config.FootstepInterval;
                }

                // update the footstep audio
                UpdateFootstepAudio();

                // run the look ahead from the feet
                Vector3 stepCheckStart = transform.position - Vector3.up * (Config.CharacterHeight * 0.5f) + (Vector3.up * 0.05f);
                Vector3 stepCheckDirection = movementVector.normalized;

                #if UNITY_EDITOR
                Vector3 DBG_StepStage1StartLoc = stepCheckStart;
                bool DBG_StepStage1Hit = false;
                bool DBG_StepStage2Hit = false;
                #endif // UNITY_EDITOR

                // first step is to check that there is a blocker
                if (Physics.Raycast(stepCheckStart, stepCheckDirection, Config.StepCheckLookAhead, Config.WalkableMask, QueryTriggerInteraction.Ignore))
                {
                    stepCheckStart += Vector3.up * Config.MaxStepUpDistance;

                    #if UNITY_EDITOR
                    DBG_StepStage1Hit = true;
                    #endif // UNITY_EDITOR

                    // now we check if the blocker is clear at the step height
                    if (!Physics.Raycast(stepCheckStart, stepCheckDirection, Config.StepCheckLookAhead, Config.WalkableMask, QueryTriggerInteraction.Ignore))
                    {                     
                        Vector3 candidatePoint = stepCheckStart + (stepCheckDirection * Config.StepCheckLookAhead);

                        // make sure we hit the ground
                        RaycastHit stepCheckHitResult;
                        if (Physics.Raycast(candidatePoint, Vector3.down, out stepCheckHitResult, Config.CharacterHeight * 0.5f, Config.WalkableMask, QueryTriggerInteraction.Ignore))
                        {
                            // trying to step to a valid spot?
                            if (Vector3.Angle(Vector3.up, stepCheckHitResult.normal) < Config.MaxSlope)
                            {
                                transform.position = stepCheckHitResult.point + Vector3.up * Config.CharacterHeight * 0.5f;
                            }
                            #if UNITY_EDITOR
                            else
                                DBG_StepStage2Hit = true;
                            #endif // UNITY_EDITOR  
                        }
                        #if UNITY_EDITOR
                        else
                            DBG_StepStage2Hit = true;
                        #endif // UNITY_EDITOR   
                    }
                    #if UNITY_EDITOR
                    else
                        DBG_StepStage2Hit = true;
                    #endif // UNITY_EDITOR                   
                }

                #if UNITY_EDITOR
                if (DEBUG_ShowStepRays)
                {
                    if (DBG_StepStage1Hit)
                        Debug.DrawLine(stepCheckStart, stepCheckStart + stepCheckDirection * Config.StepCheckLookAhead, DBG_StepStage2Hit ? Color.red : Color.green, 0f);
                    else
                        Debug.DrawLine(DBG_StepStage1StartLoc, DBG_StepStage1StartLoc + stepCheckDirection * Config.StepCheckLookAhead, DBG_StepStage1Hit ? Color.blue : Color.yellow, 0f);
                }
                #endif // UNITY_EDITOR

                // back on the ground - reset drag, physics material and jump count
                CharacterRB.drag = OriginalDrag;
                CharacterCollider.material = Config.DefaultMaterial;
                JumpCount = 0;
                IsJumping = false;
            }
        }
    }

    public float CurrentSpeed
    {
        get
        {
            return IsGrounded ? (IsRunning ? Config.RunSpeed : Config.WalkSpeed) : (Config.AirControl ? Config.InAirSpeed : 0);
        }
    }

    protected virtual Vector2 GetMovementInput()
    {
        return _Internal_MovementInput;
    }

    protected void UpdateFootstepAudio()
    {
        // update the time until the next footstep
        if (TimeUntilNextFootstep > 0)
        {
            float footstepTimeScale = 1f + Config.FootstepFrequencyWithSpeed.Evaluate(CharacterRB.velocity.magnitude / Config.RunSpeed);

            TimeUntilNextFootstep -= Time.deltaTime * footstepTimeScale;
        }

        // time to play the sound?
        if (TimeUntilNextFootstep <= 0)
        {
            OnPlayFootstepAudio?.Invoke();

            TimeUntilNextFootstep = Config.FootstepInterval;

            return;
        }
    }
    #endregion

    #region Cursor Handling
    public void UpdateCursorLock(bool newValue)
    {
        LockCursor = newValue;

        // update the cursor state
        Cursor.lockState = LockCursor ? CursorLockMode.Locked : CursorLockMode.None;
        Cursor.visible = !LockCursor;

        Config.InvertCamera_Vertical = PlayerPrefs.GetInt("Controls.InvertY", 0) == 1;
    }
    #endregion

    #region Update Toggling
    public void SetCanUpdate(bool newValue)
    {
        EnableUpdates = newValue;
    }
    #endregion

    public void IsInteracting(bool isInteracting)
    {
        CanShowSettings = !isInteracting;
        
        CharacterAnimController?.SetBool(Flag_IsWalking, !isInteracting);
        
        // Temp fix, might be a bit jarring when suddenly stopping playmovement
        CharacterAnimController.SetFloat("horizontal_f", 0f);
        CharacterAnimController.SetFloat("vertical_f", 0f);

        SetCanUpdate(!isInteracting);
        UpdateCursorLock(!isInteracting);
        Debug.Log(isInteracting);

        if (isInteracting)
        {
            CameraAnchor.transform.position = transform.position + Config.CameraHeight * Vector3.up;
            Update();
        }
    }

    #region IPausable
    public bool OnPauseRequested()  { return true; }
    public bool OnResumeRequested() { return true; }

    public void OnPause()  { }
    public void OnResume() { }
    #endregion    
}
