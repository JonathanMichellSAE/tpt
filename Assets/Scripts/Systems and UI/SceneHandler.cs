﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;

public class SceneHandler : MonoBehaviour
{
    public string nameOfGameScene;
    public string nameOfPauseGameScene; // make sure this is null in scenes we don't want pause the game

    CharacterMotor charMotor;

#pragma warning disable 0649
    [SerializeField] private string sceneName;
    [SerializeField] private List<string> scenesLoaded;
#pragma warning restore 0649

    //private SerialisationSystem serialisationSystem;

    public void Start()
    {
        //serialisationSystem = FindObjectOfType<SerialisationSystem>();

        charMotor = FindObjectOfType<CharacterMotor>();

        for (int i = 0; i < SceneManager.sceneCount; i++)
        {
            Scene scene = SceneManager.GetSceneAt(i);
            sceneName = scene.name;
            scenesLoaded.Add(sceneName);
        }  
    }

    public void PauseGameFunction()
    {
        if (nameOfPauseGameScene != null)
        {
            if (!scenesLoaded.Contains(nameOfPauseGameScene)) // Check to see if game is not paused
            {
                PauseGame(nameOfPauseGameScene);
            }
            else // Game is paused
            {
                UnpauseGame(nameOfPauseGameScene);
            }
        }
        else
        {
            Debug.Log("Either you forgot to add a name for PauseGameScene in SceneHandler or this is a scene you don't want PauseGame to run.");
        }
    }

    public void LoadScene(string SceneName)
    {
        PlayerPrefs.SetInt("saveFile", 0);
        PlayerPrefs.Save();
        SceneManager.LoadSceneAsync(SceneName);
    }

    /// <summary>
    /// Exits the application/editor play mode when this button is pressed
    /// </summary>
    public void Quit()
    {
        // If in the Unity Editor, turns off play mode
        #if UNITY_EDITOR
        {
            Debug.Log("Quitting the game...");
            UnityEditor.EditorApplication.isPlaying = false;
        }
        // If in application, exit application
        #else
        {
            Application.Quit();
        }
        #endif
    }

    /// <summary>
    /// Loads a pause scene on top of the current scene
    /// </summary>
    /// <param name="pauseScene"></param>
    public void PauseGame(string pauseScene)
    {
        Debug.Log("Pausing game...");

        charMotor.SetCanUpdate(false);
        charMotor.UpdateCursorLock(false);

        SceneManager.LoadSceneAsync(pauseScene, LoadSceneMode.Additive);
        scenesLoaded.Add(pauseScene);

        //foreach (GameObject g in SceneManager.GetActiveScene().GetRootGameObjects())
        {
            //if (!g.CompareTag("SerialisationSystem"))
            //{
            //    //g.SetActive(false);
            //}
        }
    }

    /// <summary>
    /// Unloads the pause scene
    /// </summary>
    /// <param name="pauseScene"></param>
    public void UnpauseGame(string pauseScene)
    {
        Debug.Log("Resuming game...");

        charMotor.SetCanUpdate(true);
        charMotor.UpdateCursorLock(true);

        scenesLoaded.Remove(pauseScene);
        SceneManager.UnloadSceneAsync(pauseScene);
        
        //foreach (GameObject g in SceneManager.GetActiveScene().GetRootGameObjects())
        {
            //g.SetActive(true);
        }
    }

    public void SaveGame()
    {
        //serialisationSystem.SaveData();
    }

    public void LoadGame()
    {
        if (nameOfGameScene != null)
        {
            // player prefs
            PlayerPrefs.SetInt("saveFile", 1);
            PlayerPrefs.Save();
            SceneManager.LoadSceneAsync(nameOfGameScene);
        }
        else
        {
            Debug.LogError("You haven't defined the nameOfGameScene in SceneHandler");
        }
        
    }
}
