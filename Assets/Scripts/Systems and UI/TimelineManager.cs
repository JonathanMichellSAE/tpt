using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class TimelineManager : MonoBehaviour
{
    private PlayableDirector lastPlayed;

    public void Play(PlayableDirector playableDirect)
    {
        if (lastPlayed != null)
        {
            lastPlayed.Stop();
        }

        playableDirect.Play();

        lastPlayed = playableDirect;

        Debug.Log(playableDirect.gameObject.name);
    }

    public void Stop()
    {
        if (lastPlayed != null)
        {
            lastPlayed.Stop();
            lastPlayed = null;
        }
    }
}
