﻿using UnityEngine.Audio;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour
{
    public AudioClip mainSong;
    public AudioClip transitionClip;
    public AudioSource audioSource;

    public static AudioManager audioManagerInstance;

    void Awake()
    {
        if (audioManagerInstance == null)
        {
            audioManagerInstance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
        
        audioSource = GetComponent<AudioSource>();
        audioSource.loop = true;

    }
    private void Start()
    {
        audioSource.clip = mainSong;
        audioSource.Play();
    }

    public void PlayAudioClip()
    {
        //audioSource.clip = transitionClip;
        audioSource.PlayOneShot(transitionClip);
        //Debug.Log("Played the audio clip");
    }

}
