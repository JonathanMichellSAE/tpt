using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseHandler : MonoBehaviour
{
    SceneHandler sceneHandler;
    
    // Start is called before the first frame update
    void Start()
    {
        sceneHandler = FindObjectOfType<SceneHandler>();
    }

    public void UnpauseGame()
    {
        sceneHandler.UnpauseGame(sceneHandler.nameOfPauseGameScene);
    }
}
