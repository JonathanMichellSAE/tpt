using UnityEngine;

public class CameraRotate : MonoBehaviour
{
    public float yRotationSpeed = 3f;

    void Update()
    {
        float yRotate = yRotationSpeed * Time.deltaTime;

        transform.Rotate(0, yRotate, 0);
        transform.rotation = Quaternion.Euler(0, transform.eulerAngles.y, 0);
    }
}
