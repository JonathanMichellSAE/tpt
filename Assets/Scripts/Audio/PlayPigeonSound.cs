using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayPigeonSound : MonoBehaviour
{
    public AudioSource audioSource;
    
    public AudioClip[] sounds;

    public float minWaitTime = 3f;
    public float maxWaitTime = 20f;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        
        if (!audioSource || sounds != null)
            StartCoroutine(PlaySound());
    }

    IEnumerator PlaySound()
    {
        audioSource.PlayOneShot(sounds[Random.Range(0, sounds.Length)]);
        //Debug.Log("Now playing: " + audioSource.clip);
        //yield return new WaitForSeconds(audioSource.clip.length);
        yield return new WaitForSeconds(Random.Range(minWaitTime, maxWaitTime));
        StartCoroutine(PlaySound());
    }
}
