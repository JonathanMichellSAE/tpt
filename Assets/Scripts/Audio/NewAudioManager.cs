using UnityEngine.Audio;
using UnityEngine;
using System;

public class NewAudioManager : MonoBehaviour
{
    public Sound[] sounds;

    public static NewAudioManager instantce;


    private void Awake()
    {
        if (instantce == null)
        {
            instantce = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
            return;
        }

        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }
    }

    private void Start()
    {
        PlaySound("MainTheme");
    }

    public void PlaySound (string soundName)
    {
        Sound s = Array.Find(sounds, sound => sound.name == soundName);
        
        if (s == null)
        {
            Debug.LogWarning("Sound: " + soundName + " not found!");
            return;
        }

        s.source.Play();
    }

    public void StopSound (string soundName)
    {
        Sound s = Array.Find(sounds, sound => sound.name == soundName);
        
        if (s == null)
        {
            Debug.LogWarning("Sound: " + soundName + " not found!");
            return;
        }

        s.source.Stop();
    }
}
